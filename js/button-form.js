
var button = $('#button_form');
button.on('click', buttonClickhandler);

function buttonClickhandler (event) {
   let button = $(event.currentTarget),
       input = button.siblings('input[type="file"]');
   input.trigger('click');
}